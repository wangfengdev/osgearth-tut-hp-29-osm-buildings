#pragma once

#include <osg/Node>
#include <osgEarth/GeoTransform>
#include <osg/Geode>
#include "OSMBuildingFeature.h"
#include "Building3DRings.h"
#include <string>
#include <vector>
#include <iostream>

#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Vec2d>
#include <osg/Vec3d>
#include <osg/Geometry>

#include <osg/ShapeDrawable>
#include <osgEarth/Utils> //for osgEarth::Util::ObjectStorage Class
#include <osgEarth/MapNode>

using namespace std;

//以瓦片为单位加载、保存、绘制OSM Buildings
class OSMBuildingTileNode : public osg::MatrixTransform
{
public:


	//用于更新每个建筑高程数据的回调函数，每次计算经纬度高程的计算量都很大
	struct UpdateCallback :osg::NodeCallback {
	public:
		UpdateCallback(OSMBuildingTileNode* tileNode) :_tileNode(tileNode) {}
		virtual void operator()(osg::Node* node, osg::NodeVisitor* nv);
	protected:
		osg::observer_ptr<OSMBuildingTileNode> _tileNode;
	};

public:
	OSMBuildingTileNode( const string& name,double centerLon,double centerLat, string& jsonTextOrOgrFilename);

	//this constructor is for new pbf. usePbf should always be true.
	OSMBuildingTileNode(const string& name, double centerLon, double centerLat, string& pbfdata, const osgEarth::TileKey& key);
	virtual ~OSMBuildingTileNode();
	virtual void traverse(osg::NodeVisitor& nv); //for install TerrainCallback

	osg::Vec3d getTileCenter() { return _tileCenterPoint; }
	vector<osg::Vec2d>& getBuildingCenter2DArray() { return _osmBuildingCenter2DArray; }

	osg::Geometry* getGeometry() { return _geometry.get(); }
	/// <summary>
	/// this method will rebuild geometries and reset _geode to this child.
	/// 参数是每个建筑中心高程相对瓦片中心高程的相对高程
	/// </summary>
	void buildTheTileNode();//y

	string getName() { return _name; };//y
	
	bool setNeedUpdateHeight(double tileHeight, vector<double>& newBuiHeiArray);
	bool isNeedUpdateHeight() { return _needUpdateHeights; }//y

	static osgEarth::SpatialReference* _s_srs  ;//y


	//for MatrixTransform
	void setMatrixTransform(osg::Vec3d v);

	void recomputeBuildingHeights();


protected:
	vector<osg::Vec2d> _osmBuildingCenter2DArray;
	vector<double> _currentBuildingHeiVsTileHeiArrray;
	vector<OSMBuildingFeature> _osmBuildingFeatureArray;
	vector<unsigned short> _vertexBuildingIndexArray;// vertex's building index of VertexArray

	osg::Vec3d _tileCenterPoint;//x: lon, y:lat , z:altitude above surface.
	string _name;
	osg::ref_ptr<osg::Geode> _geode;
	osg::ref_ptr<osg::Geometry> _geometry;


	//osg::observer_ptr<TerrainCallback> _terrainCallback;//yes
	osg::observer_ptr<osgEarth::MapNode> _mapNode;//yes
	//bool _isTerrainCallbackInstalled = false;//yes

	bool _needUpdateHeights = false;//yes

	//receive sending back building heights data
	vector<double> _newBuiHeightArray;



	///deprecated ------------------------------------------------------------------------
	
	////用于获取地形更新的回调函数
	//struct TerrainCallback :public osgEarth::TerrainCallback {
	//public:
	//	TerrainCallback(OSMBuildingTileNode* tileNode) :_tileNode(tileNode) {
	//		_lastHeight = _tileNode->getTileCenter().z();
	//		_tile3857name = _tileNode->getName();
	//		_tileNodeCenter = tileNode->getTileCenter();
	//	}
	//	virtual void onTileUpdate(const osgEarth::TileKey& key, osg::Node* graph, osgEarth::TerrainCallbackContext& context);
	//protected:
	//	osg::observer_ptr<OSMBuildingTileNode> _tileNode;
	//	bool _needRebuild = true;
	//	double _lastHeight = 0;
	//	double _lastLod = 0;
	//	string _tile3857name;
	//	osg::Vec3d _tileNodeCenter;
	//	int _unchangeCount = 0;
	//};

};

