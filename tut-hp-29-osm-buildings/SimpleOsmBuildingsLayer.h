#pragma once

#include <osgEarth/SimplePager>
#include <osg/MatrixTransform>
#include <osg/ShapeDrawable>
#include <osgEarth/PagedNode>
#include <osgEarth/CullingUtils>
#include <osgEarth/URI>
#include <sstream>
#include <string>
#include "OSMBuildingFeature.h"
#include <osgEarth/MapNode>
#include "OSMBuildingTileNode.h"

/// <summary>
/// 经过四天的尝试始终没有找到将将GeoTransform放在SimplePager里面的方法。
/// GeoTransform直接或者通过几层Group放在MapNode就是一切正常，但是放在SimplePager就是不灵。
/// 我怀疑可能是 GeoTransform::traverse(osg::NodeVisitor& nv)这里面的问题，
/// 通过实验我发现在SimplePager里面的子节点没有机会执行 nv.UPDATE_VISITOR 的代码，
/// 而GeoTransform的自动定位确明显基于这个UPDATE_VISITOR的代码。
/// SimplePager内部的Node都是基于PagedNode2的。
/// 果然在PageNode2的源码里，我发现了，除了CullVisitor，其他Visitor都只会执行其子节点的traverseChildren(nv);。
/// 同时在ModelLayer看到一句话 (a) Use a terrain callback to set the Z value。就是这个了。
/// </summary>

class OSMBuildingTileNode;

class SimpleOSMBuildingsLayer:public osgEarth::SimplePager
{
public:
	struct RecomputeHeightsRequest {
		osg::observer_ptr<OSMBuildingTileNode> mTileNode;
		osg::Vec3d mTileCenter;
		vector<osg::Vec3d> mBuildingsCenter;
	};

	struct TerrainCallback :public osgEarth::TerrainCallback {
	public:
		TerrainCallback(SimpleOSMBuildingsLayer* buildingLayer) :_buildingLayer(buildingLayer) {}
		virtual void onTileUpdate(const osgEarth::TileKey& key, osg::Node* graph, osgEarth::TerrainCallbackContext& context);
	protected:
		osg::observer_ptr<SimpleOSMBuildingsLayer> _buildingLayer;
	};


public:
	SimpleOSMBuildingsLayer(const osgEarth::Map* map,osgEarth::MapNode* mapNode);
	virtual osg::ref_ptr< osg::Node > 	createNode(
		const osgEarth::TileKey& key, osgEarth::ProgressCallback* progress);
	inline void setUrl(std::string url) { _url = url; }
	inline std::string getUrl() { return _url; }

	void traverse(osg::NodeVisitor&nv);

	//onTileUpdated
	void processTerrainTileUpdated(const osgEarth::TileKey& key, osg::Node* graph, osgEarth::TerrainCallbackContext& context);

protected:
	//osg::ref_ptr<osg::Drawable> buildDrawable(std::string& jsondata, float& centerLonInDEG, float& centerLatInDEG);
	std::string _url;//should be http://.../{z}/{x}/{y}.json 
	osg::observer_ptr<osgEarth::MapNode> _mapNode;
	osg::observer_ptr<osgEarth::SpatialReference> _srs;
	//test add tilenode in traverse
	osg::ref_ptr<osgEarth::GeoTransform> _transform;

	bool _isTerrainCallbackInstalled = false;

	vector< osg::observer_ptr<OSMBuildingTileNode> > _observedTileNodes;

	OpenThreads::Mutex _mutexForObservedTileNodes;
	
	//很有趣，试了这么久问题出现在getHeight的经纬度有可能落在Terrain瓦片缝隙中间，导致无法获取height值
	//解决方法就是第一次失败，偏移一点点，算第二次，再失败那就放弃。
	bool computeHeight(const osgEarth::Terrain* ter, double lon, double lat, double& hei);
};

