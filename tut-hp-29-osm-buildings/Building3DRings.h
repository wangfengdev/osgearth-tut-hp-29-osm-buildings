#pragma once
#include <string>
#include <vector>
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Vec2d>
#include <osg/Vec3d>
#include "VertexNIndex.h"
using namespace std;

//the outter ring and interia rings of vertices of one building.
struct Building3DRings {
	vector<VertexNIndex> outRing;
	vector<vector<VertexNIndex>> innRingVec;
};
