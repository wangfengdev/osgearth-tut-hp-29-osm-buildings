#pragma once
#include <string>
#include <vector>
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Vec2d>
#include <osg/Vec3d>
//vertex and its index in the whole array.
struct VertexNIndex {
	osg::Vec3d vertex;
	unsigned short index = 0;
};

//a edge of two vertex_n_index.
struct VNIEdge {
	VertexNIndex* ptr0 = 0;
	VertexNIndex* ptr1 = 0;
};

//the bounding rectangle of a polyon, composite of four directional edge (norh, south, west, east).
struct FourDirectionVNIEdge {
	VNIEdge north, south, west, east;
};
