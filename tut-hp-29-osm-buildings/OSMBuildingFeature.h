#pragma once
#include <string>
#include <vector>
#include <sstream>
#include <osg/Vec2>
#include <osg/Vec3>
#include <osg/Vec2d>
#include <osg/Vec3d>
#include "geos.h"
#include <list>
#include <geos/geom/GeometryFactory.h>
#include <geos/geom/Geometry.h>
#include <gdal_priv.h>
#include "ogrsf_frmts.h" //for vector features
#include "VertexNIndex.h"
#include "Building3DRings.h"

#include <osgEarth/GeoMath>
#include <osgEarth/GeoTransform>
#include <osgEarth/SpatialReference>
#include <osg/FrontFace>
#include <osg/CullFace>

#include "P2tPointWithIndex.h"
#include "poly2tri/poly2tri.h"

//new for pbf
#define OSGEARTH_HAVE_MVT 1
#include <osgEarth/MVT>

const double DEG2RAD = 3.1415926 / 180.0;

using namespace std;

struct OSMBuildingFeature {
public:
	static bool ReadBuildingFeaturesFromVectorFile(string filename, vector<OSMBuildingFeature>& features);
	static bool ReadBuildingFeaturesFromPBF(string pbfdata, const osgEarth::TileKey& key,vector<OSMBuildingFeature>& features);
	static bool ReadBuildingFeaturesFromJsonText(string jsonText, vector<OSMBuildingFeature>& features);
	//compute center coordinates of whole buildings region.
	//static osg::Vec2d computeAllBuildingsCenter(const vector<OSMBuildingFeature>& obuildingArray);
	//compute one building center coordinates
	static osg::Vec2d computeBuildingCenter(const OSMBuildingFeature& obuildingFeature);

	//build vertex, color, normal arrays for drawable. I know this is odd as a static method.
	static void buildOsmBuildingGeometryWithCenterPoint(
		OSMBuildingFeature& obuildingFeature,
		const osg::Vec2d originLL,
		const double buildingBaseHeight,//建筑相对瓦片中心高程的相对高程
		osg::Vec3dArray* vertexArray,
		osg::Vec3Array* normalArray,
		osg::Vec3Array* colorArray
	);

public:
	string id;
	float totHeight = 0;//including roof height. 绝对值 24
	float minHeight = 0;//height above terrain. 绝对值 21
	float roofHeight = 0; // 屋顶高度相对值 3   totHeight=minHeight+wallHeight+roofHeight  
	// wallHeight = (totHeight - minHeight) - roofHeight 
	float roofDirection = 0;
	string roofShape;// if empty means no roof.
	osg::Vec3 color;
	osg::Vec3 roofColor;
	vector<osg::Vec2d> outRing;//Vec2.x is longitude, Vec2.y is latitude.
	vector<vector<osg::Vec2d>> innRingVec;




protected:
	//SRS
	static osg::observer_ptr<osgEarth::SpatialReference> _srs;//it's a weak pointer.

	//functions
	static double GetFieldAsDouble(OGRFeature* fea, string fieldName, double defaultValue);
	static string GetFieldAsString(OGRFeature* fea, string fieldName, string defaultValue);
	static osg::Vec3 GetFieldAsColor(OGRFeature* fea, string fieldName, osg::Vec3 defaultValue);



	
	
	//compute vertices of local coordinates from ogr features.
	static void computeRelativeCenterPoints(
		const OSMBuildingFeature& obuildingFeature,
		const osg::Vec2d& center,
		const double buildingBaseHeight,
		Building3DRings& retRings);
	//generate extrude vertices.
	static void generateExtrudedPoints(const OSMBuildingFeature& obuildingFeature,
		const Building3DRings& grndRings, Building3DRings& roofRings);
	//make wall triangles
	static void makeWallTriangleVertexIndices(const Building3DRings& grndRings, const Building3DRings& roofRings,
		vector<unsigned short>& wallTriIndices,
		vector<float>& colorPercents);
	//compute center coordinates from array of vertex_n_index.
	static osg::Vec2d  computeNVIRingCenter(vector<VertexNIndex>& vniArray);
	//compute four directional edge of a ring.
	static FourDirectionVNIEdge calculateFourDirectionEdges(vector<VertexNIndex>& verts);
	//edit skillion roof vertices if necessary.
	static void modifySkillionRoofVertexHeightByRoofDirection(float roofDirectionDeg, float roofHeight, vector<VertexNIndex>& verts);
	//try to simplelize a polygon more than 5 vertices if necessary.
	static vector<VertexNIndex> trySimpleizePolygon(vector<VertexNIndex>& inRing);
	//generate roof triangle mesh.
	static void makeRoofVerticesAndIndices(
		const OSMBuildingFeature& obuilding,
		Building3DRings& roofRings,
		vector<VertexNIndex>& newVerts,
		vector<unsigned short>& newIndices,
		vector<float>& colorPercents);
	//compute normal by three vertices.
	static osg::Vec3 computeOneNormal(osg::Vec3d* v0, osg::Vec3d* v1, osg::Vec3d* v2);

};
