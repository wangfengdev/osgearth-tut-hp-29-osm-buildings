#include "SimpleOsmBuildingsLayer.h"

SimpleOSMBuildingsLayer::SimpleOSMBuildingsLayer(
    const osgEarth::Map* map
    , osgEarth::MapNode* mapNode)
:SimplePager(map, osgEarth::Profile::create("spherical-mercator"))
,_mapNode(mapNode)
{
	setMinLevel(15);
	setMaxLevel(15);//resolution is 4.777m , each tile cover 1.222x1.222KM
	setRangeFactor(3);//covers about 1.222*3 x 1.222*3 KM

    ////new PBF
    //setMinLevel(14);
    //setMaxLevel(14);
    //setRangeFactor(3);

    //this is for getHeight not for tile
    _srs = osgEarth::SpatialReference::get("wgs84");
}

void stringReplace(std::string& input, std::string oldstr, std::string newstr) {
    size_t pos0 = input.find(oldstr);
    if (pos0 != std::string::npos ) {
        input.replace(pos0, oldstr.length(), newstr);
    }
}

//据某种还没有看懂的SimplePager机制，它是在外边使用了Pager loading，在下面这个函数只负责加载数据即可。
osg::ref_ptr< osg::Node > SimpleOSMBuildingsLayer::createNode(
	const osgEarth::TileKey& key, osgEarth::ProgressCallback* progress) 
{
    //osg::BoundingSphered bounds = getBounds(key);

    std::string turl = this->getUrl();
    std::stringstream ssz, ssx, ssy;
    ssz << key.getLOD();
    ssx << key.getTileX();
    ssy << key.getTileY();//for geojson
    ////level14 tileYcount = 16384
    //int invYForMyServices = 16384 - key.getTileY();
    //ssy << invYForMyServices; //for pbf

    stringReplace(turl, "{z}", ssz.str());
    stringReplace(turl, "{y}", ssy.str());
    stringReplace(turl, "{x}", ssx.str());
    
    double centerLon = 0;
    double centerLat = 0;
    osgEarth::GeoExtent gExtentWgs84 = key.getExtent().transform(_srs.get());
    gExtentWgs84.getCentroid(centerLon, centerLat);
    if (key.getLOD() == 15)
    {
        osgEarth::URI theUri(turl);

        //JSON
        std::string jsondata = theUri.getString();
        if (jsondata.length() > 10) {
            osg::ref_ptr<OSMBuildingTileNode> tileNode = new OSMBuildingTileNode(
                key.str(), centerLon, centerLat, jsondata);
            _mutexForObservedTileNodes.lock();
            osg::observer_ptr<OSMBuildingTileNode> weakPtr(tileNode.get());
            _observedTileNodes.push_back(weakPtr);
            _mutexForObservedTileNodes.unlock();
            return tileNode.release();
        }
    }
    //if (key.getLOD() == 14)
    //{
    //    ////new PBF
    //    //std::string pbfdata = theUri.getString();
    //    //if (pbfdata.length() > 10) {
    //    //    osg::ref_ptr<OSMBuildingTileNode> tileNode = new OSMBuildingTileNode(
    //    //        key.str(), centerLon, centerLat, pbfdata, key);
    //    //    _mutexForObservedTileNodes.lock();
    //    //    osg::observer_ptr<OSMBuildingTileNode> weakPtr(tileNode.get());
    //    //    _observedTileNodes.push_back(weakPtr);
    //    //    _mutexForObservedTileNodes.unlock();
    //    //    return tileNode.release();
    //    //}
    //}
    return nullptr;
}

void SimpleOSMBuildingsLayer::traverse(osg::NodeVisitor& nv)
{
    if (nv.getVisitorType() == nv.UPDATE_VISITOR) {
        //clean the dealloc tilenodes
        {
            _mutexForObservedTileNodes.lock();
            vector< osg::observer_ptr<OSMBuildingTileNode> > aliveTileNodes;
            for (auto it = _observedTileNodes.begin(); it != _observedTileNodes.end(); ++it) {
                if (it->valid()) aliveTileNodes.push_back(*it);
            }
            _observedTileNodes = aliveTileNodes;
            _mutexForObservedTileNodes.unlock();
        }
        
        //install layer terrain callback.
        if (_isTerrainCallbackInstalled == false) {

            if (_mapNode->getTerrain()) {
                osg::ref_ptr<TerrainCallback> tcallback = new TerrainCallback(this);
                _mapNode->getTerrain()->addTerrainCallback(tcallback.get());
                _isTerrainCallbackInstalled = true;
            }
        }
    }
    osgEarth::SimplePager::traverse(nv);
}


void SimpleOSMBuildingsLayer::TerrainCallback::onTileUpdate(
    const osgEarth::TileKey& key, 
    osg::Node* graph, 
    osgEarth::TerrainCallbackContext& context)
{
    if (_buildingLayer.valid()) {
        osg::ref_ptr<SimpleOSMBuildingsLayer> layer;
        if (_buildingLayer.lock(layer)) {
            layer->processTerrainTileUpdated(key, graph, context);
        }
    }



}

void SimpleOSMBuildingsLayer::processTerrainTileUpdated(
    const osgEarth::TileKey& key, osg::Node* graph, osgEarth::TerrainCallbackContext& context)
{
    _mutexForObservedTileNodes.lock();
    vector< osg::observer_ptr<OSMBuildingTileNode> > temp_observedTileNodes=_observedTileNodes;
    _mutexForObservedTileNodes.unlock();
    for (auto it = temp_observedTileNodes.begin(); it != temp_observedTileNodes.end(); ++it) {
        if (it->valid()) {
            osg::ref_ptr<OSMBuildingTileNode> tileNode;
            if (it->lock(tileNode)) {
                osg::Vec3d tileCenter = tileNode->getTileCenter();
                if (key.getExtent().contains(tileCenter)) 
                {//这里只判断瓦片中心坐标是否落在Key范围内

                    double tileMsl = 0;
                    double tileHei = 0;
                    if ( computeHeight( context.getTerrain(), tileCenter.x(), tileCenter.y(), tileHei) )
                    {
                        if (fabs(tileHei - tileCenter.z())>1.0) {
                            //如果高程变化大于1m则更新该瓦片建筑高度数据
                            tileCenter.z() = tileHei;
                            vector<osg::Vec2d> buildingCenterArray = tileNode->getBuildingCenter2DArray();
                            vector<double> buiHeiArray ;
                            buiHeiArray.reserve(buildingCenterArray.size());
                            double buiMsl = 0;
                            double buiHei = 0;
                            for (auto icenter = buildingCenterArray.begin(); icenter != buildingCenterArray.end(); ++icenter) {
                                if ( computeHeight(context.getTerrain(),
                                    icenter->x(), icenter->y(),buiHei )
                                    )
                                {
                                    buiHeiArray.push_back(buiHei - tileHei);//这里保留的是相对瓦片中心的相对高度
                                }
                                else {
                                    buiHeiArray.push_back(0);
                                }
                            }//每个建筑循环结束

                            //发回高度数据
                            tileNode->setNeedUpdateHeight(tileHei, buiHeiArray);

                        }//中心高度变化大于1米
                    }//计算中心高度OK
                    else {
                        
                    }
                }//瓦片在key内部
            }
        }
    }
}

bool SimpleOSMBuildingsLayer::computeHeight(const osgEarth::Terrain* ter, double lon, double lat, double& hei)
{
    double msl = 0;
    bool ok = ter->getHeight(OSMBuildingTileNode::_s_srs,
        lon,
        lat, &msl, &hei);
    if (!ok) {
        lon += 0.00001;
        lat += 0.00001;
        ok = ter->getHeight(OSMBuildingTileNode::_s_srs,
            lon,
            lat, &msl, &hei);
        if (!ok) {
            lon -= 0.00002;
            lat -= 0.00002;
            ok = ter->getHeight(OSMBuildingTileNode::_s_srs,
                lon,
                lat, &msl, &hei);
        }
    }
    return ok;
}