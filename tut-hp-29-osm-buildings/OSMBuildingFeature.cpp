#include "OSMBuildingFeature.h"

osg::observer_ptr<osgEarth::SpatialReference> OSMBuildingFeature::_srs= osgEarth::SpatialReference::get("wgs84");

double OSMBuildingFeature::GetFieldAsDouble(OGRFeature* fea, string fieldName, double defaultValue) {
	int index1 = fea->GetFieldIndex(fieldName.c_str());
	if (index1 < 0) return defaultValue;
	if (fea->IsFieldSetAndNotNull(index1)) {
		return fea->GetFieldAsDouble(index1);
	}
	else {
		return defaultValue;
	}
}
string OSMBuildingFeature::GetFieldAsString(OGRFeature* fea, string fieldName, string defaultValue) {
	int index1 = fea->GetFieldIndex(fieldName.c_str());
	if (index1 < 0) return defaultValue;
	if (fea->IsFieldSetAndNotNull(index1)) {
		return string(fea->GetFieldAsString(index1));
	}
	else {
		return defaultValue;
	}
}
/// <summary>
/// 
/// </summary>
/// <param name="fea"></param>
/// <param name="fieldName"></param>
/// <param name="defaultValue"></param>
/// <returns> RGB value, each component is from 0.0 to 1.0.</returns>
osg::Vec3 OSMBuildingFeature::GetFieldAsColor(OGRFeature* fea, string fieldName, osg::Vec3 defaultValue) {
	string str1 = GetFieldAsString(fea, fieldName, "");
	if (str1 == "") {
		return defaultValue;
	}
	else {
		unsigned int r, g, b;
		std::stringstream ss;
		ss << std::hex << str1.substr(1, 2);
		ss >> r; ss.clear();
		ss << std::hex << str1.substr(3, 2);
		ss >> g; ss.clear();
		ss << std::hex << str1.substr(5, 2);
		ss >> b; ss.clear();
		osg::Vec3 color1;
		color1[0] = r / 255.f;
		color1[1] = g / 255.f;
		color1[2] = b / 255.f;
		return color1;
	}
}



bool OSMBuildingFeature::ReadBuildingFeaturesFromVectorFile(string filename, vector<OSMBuildingFeature>& features) {
	GDALAllRegister();//for vector
	GDALDataset* dataset = (GDALDataset*)GDALOpenEx(filename.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL);
	if (dataset == NULL)return false;
	OGRLayer* layer = dataset->GetLayer(0);
	if (layer == 0) return false;
	for (auto& feature1 : layer)
	{
		OSMBuildingFeature ofeature;
		ofeature.id = GetFieldAsString(&(*feature1), "id", "noid");
		ofeature.totHeight = GetFieldAsDouble(&(*feature1), "height", 0);
		ofeature.minHeight = GetFieldAsDouble(&(*feature1), "minHeight", 0);
		ofeature.roofHeight = GetFieldAsDouble(&(*feature1), "roofHeight", 0);
		ofeature.roofDirection = GetFieldAsDouble(&(*feature1), "roofDirection", 0);
		ofeature.roofShape = GetFieldAsString(&(*feature1), "roofShape", "");
		ofeature.color = GetFieldAsColor(&(*feature1), "color", osg::Vec3(0.8, 0.8, 0.8));
		ofeature.roofColor = GetFieldAsColor(&(*feature1), "roofColor", ofeature.color);
		//geometry
		OGRGeometry* geom = feature1->GetGeometryRef();
		if (geom != NULL && wkbFlatten(geom->getGeometryType()) == wkbPolygon)
		{
			OGRPolygon* poly = geom->toPolygon();
			OGRLinearRing* outRing = poly->getExteriorRing();//暂时不考虑内环裁剪的问题
			for (int ipt = 0; ipt < outRing->getNumPoints(); ++ipt) {
				OGRPoint opoint;
				outRing->getPoint(ipt, &opoint);
				ofeature.outRing.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
			}
			int innerRingCount = poly->getNumInteriorRings();
			for (int iinner = 0; iinner < innerRingCount; ++iinner) {
				OGRLinearRing* innRing = poly->getInteriorRing(iinner);
				vector<osg::Vec2d> ring2;
				for (int ipt = 0; ipt < innRing->getNumPoints(); ++ipt) {
					OGRPoint opoint;
					innRing->getPoint(ipt, &opoint);
					ring2.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
				}
				ofeature.innRingVec.push_back(ring2);
			}
		}
		features.push_back(ofeature);
	}
	GDALClose(dataset);
	dataset = 0;
	return true;
}


bool OSMBuildingFeature::ReadBuildingFeaturesFromJsonText(string jsonText, vector<OSMBuildingFeature>& features) {
	GDALAllRegister();//for vector
	GDALDataset* dataset = (GDALDataset*)GDALOpenEx(jsonText.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL);
	if (dataset == NULL)return false;
	OGRLayer* layer = dataset->GetLayer(0);
	if (layer == 0) return false;
	for (auto& feature1 : layer)
	{
		OSMBuildingFeature ofeature;
		ofeature.id = GetFieldAsString(&(*feature1), "id", "noid");
		ofeature.totHeight = GetFieldAsDouble(&(*feature1), "height", 0);
		ofeature.minHeight = GetFieldAsDouble(&(*feature1), "minHeight", 0);
		ofeature.roofHeight = GetFieldAsDouble(&(*feature1), "roofHeight", 0);
		ofeature.roofDirection = GetFieldAsDouble(&(*feature1), "roofDirection", 0);
		ofeature.roofShape = GetFieldAsString(&(*feature1), "roofShape", "");
		ofeature.color = GetFieldAsColor(&(*feature1), "color", osg::Vec3(0.86, 0.82, 0.78));
		ofeature.roofColor = GetFieldAsColor(&(*feature1), "roofColor", ofeature.color);
		//geometry
		OGRGeometry* geom = feature1->GetGeometryRef();
		if (geom != NULL && wkbFlatten(geom->getGeometryType()) == wkbPolygon)
		{
			OGRPolygon* poly = geom->toPolygon();
			OGRLinearRing* outRing = poly->getExteriorRing();//暂时不考虑内环裁剪的问题
			for (int ipt = 0; ipt < outRing->getNumPoints(); ++ipt) {
				OGRPoint opoint;
				outRing->getPoint(ipt, &opoint);
				ofeature.outRing.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
			}
			int innerRingCount = poly->getNumInteriorRings();
			for (int iinner = 0; iinner < innerRingCount; ++iinner) {
				OGRLinearRing* innRing = poly->getInteriorRing(iinner);
				vector<osg::Vec2d> ring2;
				for (int ipt = 0; ipt < innRing->getNumPoints(); ++ipt) {
					OGRPoint opoint;
					innRing->getPoint(ipt, &opoint);
					ring2.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
				}
				ofeature.innRingVec.push_back(ring2);
			}
		}
		features.push_back(ofeature);
	}
	GDALClose(dataset);
	dataset = 0;
	return true;
}

bool OSMBuildingFeature::ReadBuildingFeaturesFromPBF(string pbfdata, const osgEarth::TileKey& key, vector<OSMBuildingFeature>& features)
{
	std::stringstream in(pbfdata);

	osgEarth::FeatureList features0;
	bool mvtok = osgEarth::MVT::readTile(in, key, features0);

	//for (auto& feature1 : layer)
	//{
	//	OSMBuildingFeature ofeature;
	//	ofeature.id = GetFieldAsString(&(*feature1), "id", "noid");
	//	ofeature.totHeight = GetFieldAsDouble(&(*feature1), "height", 0);
	//	ofeature.minHeight = GetFieldAsDouble(&(*feature1), "minHeight", 0);
	//	ofeature.roofHeight = GetFieldAsDouble(&(*feature1), "roofHeight", 0);
	//	ofeature.roofDirection = GetFieldAsDouble(&(*feature1), "roofDirection", 0);
	//	ofeature.roofShape = GetFieldAsString(&(*feature1), "roofShape", "");
	//	ofeature.color = GetFieldAsColor(&(*feature1), "color", osg::Vec3(0.86, 0.82, 0.78));
	//	ofeature.roofColor = GetFieldAsColor(&(*feature1), "roofColor", ofeature.color);
	//	//geometry
	//	OGRGeometry* geom = feature1->GetGeometryRef();
	//	if (geom != NULL && wkbFlatten(geom->getGeometryType()) == wkbPolygon)
	//	{
	//		OGRPolygon* poly = geom->toPolygon();
	//		OGRLinearRing* outRing = poly->getExteriorRing();//暂时不考虑内环裁剪的问题
	//		for (int ipt = 0; ipt < outRing->getNumPoints(); ++ipt) {
	//			OGRPoint opoint;
	//			outRing->getPoint(ipt, &opoint);
	//			ofeature.outRing.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
	//		}
	//		int innerRingCount = poly->getNumInteriorRings();
	//		for (int iinner = 0; iinner < innerRingCount; ++iinner) {
	//			OGRLinearRing* innRing = poly->getInteriorRing(iinner);
	//			vector<osg::Vec2d> ring2;
	//			for (int ipt = 0; ipt < innRing->getNumPoints(); ++ipt) {
	//				OGRPoint opoint;
	//				innRing->getPoint(ipt, &opoint);
	//				ring2.push_back(osg::Vec2d(opoint.getX(), opoint.getY()));
	//			}
	//			ofeature.innRingVec.push_back(ring2);
	//		}
	//	}
	//	features.push_back(ofeature);
	//}

	return true;
}




osg::Vec2d OSMBuildingFeature::computeBuildingCenter(const OSMBuildingFeature& obuildingFeature) {
	assert(obuildingFeature.outRing.size() > 3);
	osg::Vec2d center(0, 0);
	float minx = obuildingFeature.outRing.cbegin()->x();
	float maxx = obuildingFeature.outRing.cbegin()->x();
	float miny = obuildingFeature.outRing.cbegin()->y();
	float maxy = obuildingFeature.outRing.cbegin()->y();
	for (auto it = obuildingFeature.outRing.cbegin(); it != obuildingFeature.outRing.cend(); ++it) {
		minx = MIN(minx, it->x());
		maxx = MAX(maxx, it->x());
		miny = MIN(miny, it->y());
		maxy = MAX(maxy, it->y());
	}
	center.x() = (minx + maxx) / 2.f;
	center.y() = (miny + maxy) / 2.f;
	return center;
}

//// this is not used
//osg::Vec2d OSMBuildingFeature::computeAllBuildingsCenter(const vector<OSMBuildingFeature>& obuildingArray) {
//	osg::Vec2d center(0, 0);
//	for (auto it = obuildingArray.cbegin(); it != obuildingArray.cend(); ++it) {
//		osg::Vec2d tcenter = computeBuildingCenter(*it);
//		center.x() += tcenter.x();
//		center.y() += tcenter.y();
//	}
//	assert(obuildingArray.size() > 0);
//	center.x() /= obuildingArray.size();
//	center.y() /= obuildingArray.size();
//	return center;
//}



void OSMBuildingFeature::computeRelativeCenterPoints(
	const OSMBuildingFeature& obuildingFeature, 
	const osg::Vec2d& center, 
	const double buildingBaseHeight,
	Building3DRings& retRings) {

	//current building center
	osg::Vec2d currBuildingCenterLL = computeBuildingCenter(obuildingFeature);
	//outter
	unsigned short currindex = 0;
	const double centerLatRad = center.y() * DEG2RAD;
	const double centerLonRad = center.x() * DEG2RAD;
	for (auto it = obuildingFeature.outRing.cbegin(); it != obuildingFeature.outRing.cend(); ++it) {
		double alatRad = it->y() * DEG2RAD;
		double alonRad = it->x() * DEG2RAD;
		double distx = osgEarth::GeoMath::distance(centerLatRad, centerLonRad, centerLatRad, alonRad);
		if (alonRad < centerLonRad) distx = -distx;
		double disty = osgEarth::GeoMath::distance(centerLatRad, centerLonRad, alatRad, centerLonRad);
		if (alatRad < centerLatRad) disty = -disty;
		VertexNIndex vi;
		vi.vertex.x() = distx;
		vi.vertex.y() = disty;
		vi.vertex.z() = obuildingFeature.minHeight + buildingBaseHeight ;
		vi.index = currindex++;
		retRings.outRing.push_back(vi);
	}
	//inner
	for (int iinner = 0; iinner < obuildingFeature.innRingVec.size(); ++iinner) {
		vector<VertexNIndex> viArray;
		for (auto it = obuildingFeature.innRingVec[iinner].cbegin();
			it != obuildingFeature.innRingVec[iinner].cend();
			++it)
		{
			double alatRad = it->y() * DEG2RAD;
			double alonRad = it->x() * DEG2RAD;
			double distx = osgEarth::GeoMath::distance(centerLatRad, centerLonRad, centerLatRad, alonRad);
			if (alonRad < centerLonRad) distx = -distx;
			double disty = osgEarth::GeoMath::distance(centerLatRad, centerLonRad, alatRad, centerLonRad);
			if (alatRad < centerLatRad) disty = -disty;
			VertexNIndex vi;
			vi.vertex.x() = distx;
			vi.vertex.y() = disty;
			vi.vertex.z() = obuildingFeature.minHeight+ buildingBaseHeight;
			vi.index = currindex++;
			viArray.push_back(vi);
		}
		retRings.innRingVec.push_back(viArray);
	}
}


/// <summary>
/// 如果wallheight( wallheight=totheight-minheight-roofHeight )小于1.0表示没有墙，单独就是一个屋顶，
/// 此时直接把地面点复制给顶部点，后续构建墙的时候就跳过不做墙直接做屋顶。
/// </summary>
/// <param name="obuildingFeature"></param>
/// <param name="grndRings"></param>
/// <param name="roofRings"></param>
void OSMBuildingFeature::generateExtrudedPoints(const OSMBuildingFeature& obuildingFeature,
	const Building3DRings& grndRings, Building3DRings& roofRings)
{
	double relWallHeight = obuildingFeature.totHeight - obuildingFeature.minHeight - obuildingFeature.roofHeight;
	if (relWallHeight < 1.0) relWallHeight = 0.0;//如果墙的高度小于1认为该建筑为单独屋顶，不做墙的构建。
	unsigned short currindex = grndRings.outRing.size();
	for (int ii = 0; ii < grndRings.innRingVec.size(); ++ii) {
		currindex += grndRings.innRingVec[ii].size();
	}
	for (auto it = grndRings.outRing.cbegin(); it != grndRings.outRing.cend(); ++it) {
		VertexNIndex vi = *it;
		vi.vertex.z() += relWallHeight;
		vi.index = currindex++;
		roofRings.outRing.push_back(vi);
	}
	//inner
	for (int iinner = 0; iinner < grndRings.innRingVec.size(); ++iinner) {
		vector<VertexNIndex> viArray;
		for (auto it = grndRings.innRingVec[iinner].cbegin();
			it != grndRings.innRingVec[iinner].cend();
			++it)
		{
			VertexNIndex vi = *it;
			vi.vertex.z() += relWallHeight;
			vi.index = currindex++;
			viArray.push_back(vi);
		}
		roofRings.innRingVec.push_back(viArray);
	}
}



/// <summary>
/// vertex order is outring.v0,...,outring.vn,inring0.v0,...,inring0.vn,inringN.v0,...,inringN.vn
/// wallheight<1.0 不建墙面
/// </summary>
/// <param name="grndRings"></param>
/// <param name="roofRings"></param>
/// <param name="wallTriIndices"></param>
void OSMBuildingFeature::makeWallTriangleVertexIndices(
	const Building3DRings& grndRings, 
	const Building3DRings& roofRings,
	vector<unsigned short>& wallTriIndices,
	vector<float>& colorPercents)
{
	double relWallHeight = roofRings.outRing[0].vertex.z() - grndRings.outRing[0].vertex.z();
	if (relWallHeight < 1.0) { return; } //如果墙的高度小于1认为该建筑为单独屋顶，不做墙的构建。
	//unsigned short gnv = grndRings.outRing.size();//gnv short for ground number of verts.
	//for (int ii = 0; ii < grndRings.innRingVec.size(); ++ii) gnv += grndRings.innRingVec[ii].size();

	int outFaceNum = grndRings.outRing.size() - 1;
	for (int iface = 0; iface < outFaceNum; ++iface) {
		int ifacenext = iface + 1;
		if (ifacenext == outFaceNum) ifacenext = 0;
		unsigned short btm0 = grndRings.outRing[iface].index;
		unsigned short btm1 = grndRings.outRing[ifacenext].index;
		unsigned short top0 = roofRings.outRing[iface].index;
		unsigned short top1 = roofRings.outRing[ifacenext].index;
		// 注意osmbuildings的geojson中多边形当第三轴指向纸面内的时候是右手逆时针
		//tri-1 逆时针
		wallTriIndices.push_back(btm0);
		wallTriIndices.push_back(top0);
		wallTriIndices.push_back(top1);
		colorPercents.push_back(0.5);
		colorPercents.push_back(1);
		colorPercents.push_back(1);
		//tri-2 逆时针
		wallTriIndices.push_back(btm0);
		wallTriIndices.push_back(top1);
		wallTriIndices.push_back(btm1);
		colorPercents.push_back(0.5);
		colorPercents.push_back(1);
		colorPercents.push_back(0.5);
	}
	//interia walls
	for (int iin = 0; iin < grndRings.innRingVec.size(); ++iin)
	{
		int infacenum = grndRings.innRingVec[iin].size() - 1;
		const vector<VertexNIndex>& theRing0 = grndRings.innRingVec[iin];
		const vector<VertexNIndex>& theRing1 = roofRings.innRingVec[iin];

		for (int iface = 0; iface < infacenum; ++iface) {
			int ifacenext = iface + 1;
			if (ifacenext == infacenum) ifacenext = 0;
			unsigned short btm0 = theRing0[iface].index;
			unsigned short btm1 = theRing0[ifacenext].index;
			unsigned short top0 = theRing1[iface].index;
			unsigned short top1 = theRing1[ifacenext].index;
			// 注意osmbuildings的geojson中多边形当第三轴指向纸面内的时候是右手逆时针
			//tri-1 逆时针
			wallTriIndices.push_back(btm0);
			wallTriIndices.push_back(top0);
			wallTriIndices.push_back(top1);
			colorPercents.push_back(0.5);
			colorPercents.push_back(1);
			colorPercents.push_back(1);
			//tri-2 逆时针
			wallTriIndices.push_back(btm0);
			wallTriIndices.push_back(top1);
			wallTriIndices.push_back(btm1);
			colorPercents.push_back(0.5);
			colorPercents.push_back(1);
			colorPercents.push_back(0.5);
		}
	}
}



osg::Vec2d  OSMBuildingFeature::computeNVIRingCenter(vector<VertexNIndex>& vniArray) {
	double sumx = 0;
	double sumy = 0;
	for (int iv = 0; iv < vniArray.size() - 1; ++iv)
	{
		const osg::Vec3d& vert = vniArray[iv].vertex;
		sumx += vert.x();
		sumy += vert.y();
	}
	return osg::Vec2d(sumx / (vniArray.size() - 1), sumy / (vniArray.size() - 1));
}



/// <summary>
/// verts是外环，只能有五个点，按z指向纸面外顺时针组织.
/// </summary>
/// <param name="verts"></param>
/// <returns></returns>
FourDirectionVNIEdge OSMBuildingFeature::calculateFourDirectionEdges(vector<VertexNIndex>& verts) {
	assert(verts.size() == 5);
	FourDirectionVNIEdge result;
	VertexNIndex* pNorth = &verts[0];
	VertexNIndex* pSouth = &verts[0];
	VertexNIndex* pWest = &verts[0];
	VertexNIndex* pEast = &verts[0];
	int inorth = 0;
	int isouth = 0;
	int iwest = 0;
	int ieast = 0;
	for (int iv = 1; iv < verts.size() - 1; ++iv) {
		if (pNorth->vertex.y() < verts[iv].vertex.y()) {
			pNorth = &verts[iv];
			inorth = iv;
		}
		if (pSouth->vertex.y() > verts[iv].vertex.y()) {
			pSouth = &verts[iv];
			isouth = iv;
		}
		if (pWest->vertex.x() > verts[iv].vertex.x()) {
			pWest = &verts[iv];
			iwest = iv;
		}
		if (pEast->vertex.x() < verts[iv].vertex.x()) {
			pEast = &verts[iv];
			ieast = iv;
		}
	}
	int neighbor1[] = { 1,2,3,0 };
	int neighbor2[] = { 3,0,1,2 };
	if (fabsf(pNorth->vertex.y() - verts[neighbor1[inorth]].vertex.y()) < fabsf(pNorth->vertex.y() - verts[neighbor2[inorth]].vertex.y())) {
		result.north.ptr0 = pNorth;
		result.north.ptr1 = &verts[neighbor1[inorth]];
	}
	else {
		result.north.ptr0 = pNorth;
		result.north.ptr1 = &verts[neighbor2[inorth]];
	}
	if (fabsf(pSouth->vertex.y() - verts[neighbor1[isouth]].vertex.y()) < fabsf(pSouth->vertex.y() - verts[neighbor2[isouth]].vertex.y())) {
		result.south.ptr0 = pSouth;
		result.south.ptr1 = &verts[neighbor1[isouth]];
	}
	else {
		result.south.ptr0 = pSouth;
		result.south.ptr1 = &verts[neighbor2[isouth]];
	}
	if (fabsf(pWest->vertex.x() - verts[neighbor1[iwest]].vertex.x()) < fabsf(pWest->vertex.x() - verts[neighbor2[iwest]].vertex.x())) {
		result.west.ptr0 = pWest;
		result.west.ptr1 = &verts[neighbor1[iwest]];
	}
	else {
		result.west.ptr0 = pWest;
		result.west.ptr1 = &verts[neighbor2[iwest]];
	}
	if (fabsf(pEast->vertex.x() - verts[neighbor1[ieast]].vertex.x()) < fabsf(pEast->vertex.x() - verts[neighbor2[ieast]].vertex.x())) {
		result.east.ptr0 = pEast;
		result.east.ptr1 = &verts[neighbor1[ieast]];
	}
	else {
		result.east.ptr0 = pEast;
		result.east.ptr1 = &verts[neighbor2[ieast]];
	}
	return result;
}


void OSMBuildingFeature::modifySkillionRoofVertexHeightByRoofDirection(
	float roofDirectionDeg, float roofHeight, vector<VertexNIndex>& verts) {
	FourDirectionVNIEdge fourside = calculateFourDirectionEdges(verts);
	if (roofDirectionDeg <= 45 || roofDirectionDeg > 315) {
		fourside.south.ptr0->vertex.z() += roofHeight;
		fourside.south.ptr1->vertex.z() += roofHeight;
	}
	else if (roofDirectionDeg > 45 && roofDirectionDeg <= 135) {
		fourside.west.ptr0->vertex.z() += roofHeight;
		fourside.west.ptr1->vertex.z() += roofHeight;
	}
	else if (roofDirectionDeg > 135 && roofDirectionDeg < 225) {
		fourside.north.ptr0->vertex.z() += roofHeight;
		fourside.north.ptr1->vertex.z() += roofHeight;
	}
	else {
		fourside.east.ptr0->vertex.z() += roofHeight;
		fourside.east.ptr1->vertex.z() += roofHeight;
	}
}


/// <summary>
/// 尝试将一个多边形简化，砍掉中间不影响形状多余的点。点到线段距离小于20cm就砍掉。
/// </summary>
/// <param name="inRing"></param>
/// <returns></returns>
vector<VertexNIndex> OSMBuildingFeature::trySimpleizePolygon(vector<VertexNIndex>& inRing) {
	assert(inRing.size() > 5);
	list<VertexNIndex> vnilist;
	for (int iv = 0; iv < inRing.size() - 1; ++iv) {
		vnilist.push_back(inRing[iv]);
	}
	GeometryFactory factory;
	auto nodex = vnilist.begin();
	while (true)
	{
		int lastLen = vnilist.size();

		for (int iv = 0; iv < lastLen; ++iv)
		{
			auto prev = (nodex == vnilist.begin()) ? --vnilist.end() : std::prev(nodex);
			auto next = (nodex == --vnilist.end()) ? vnilist.begin() : std::next(nodex);
			CoordinateArraySequence cl;
			cl.add(Coordinate(prev->vertex.x(), prev->vertex.y()));
			cl.add(Coordinate(next->vertex.x(), next->vertex.y()));
			geos::geom::LineString* ls = factory.createLineString(cl);
			geos::geom::Point* pt = factory.createPoint(Coordinate(nodex->vertex.x(), nodex->vertex.y()));
			double dist = fabs(pt->distance(ls));
			factory.destroyGeometry(ls);
			factory.destroyGeometry(pt);
			if (dist < 0.2) {
				vnilist.erase(nodex);
				nodex = vnilist.begin();
				if (vnilist.size() == 4) break;
			}
			else {
				nodex = next;
			}
		}
		if (vnilist.size() == 4) break;
		if (lastLen == vnilist.size()) break;
	}

	vector<VertexNIndex> result;
	for (auto it = vnilist.begin(); it != vnilist.end(); ++it) {
		result.push_back(*it);
	}
	result.push_back(vnilist.front());
	return result;
}



/// <summary>
/// 1.根据roof type判断是否需要加点，如果加点就追加到 newVerts 后面，这些点同时获得对应的 indices。
/// 2.构建房顶的三角形网，并生成索引点数组 newIndices。
/// </summary>
/// <param name="obuilding"></param>
/// <param name="grndRings"></param>
/// <param name="roofRings"></param>
/// <param name="newVertices"></param>
/// <param name="roofIndices"></param>
void OSMBuildingFeature::makeRoofVerticesAndIndices(
	const OSMBuildingFeature& obuilding,
	Building3DRings& roofRings,
	vector<VertexNIndex>& newVerts,
	vector<unsigned short>& newIndices,
	vector<float>& colorPercents)
{
	bool roofok = false;
	int currindex = roofRings.outRing.size() * 2;// multiply 2 for counting on groundRings
	for (int ihole = 0; ihole < roofRings.innRingVec.size(); ++ihole)currindex += roofRings.innRingVec[ihole].size() * 2;//counting on groundRings
	if (roofRings.innRingVec.size() == 0 && obuilding.roofHeight > 0.1 && roofRings.outRing.size() > 4) {
		if (
			obuilding.roofShape == "gabled"
			|| obuilding.roofShape == "hipped"
			|| obuilding.roofShape == "half-hipped"
			|| obuilding.roofShape == "hipped-and-gabled"
			|| obuilding.roofShape == "gambrel"
			|| obuilding.roofShape == "mansard"
			|| obuilding.roofShape == "crosspitched"
			|| obuilding.roofShape == "side_hipped"
			|| obuilding.roofShape == "side_half-hipped"
			|| obuilding.roofShape == "gabled_height_moved"
			)
		{
			//看了osmbuilding的建模，他们就是只做5个点的建筑倾斜屋顶
			if (roofRings.outRing.size() == 5) {
				FourDirectionVNIEdge fourside = calculateFourDirectionEdges(roofRings.outRing);
				float northsouthDist = fabsf(fourside.north.ptr0->vertex.y() - fourside.south.ptr0->vertex.y());
				float eastwestDist = fabsf(fourside.east.ptr0->vertex.x() - fourside.west.ptr0->vertex.x());
				if (northsouthDist > eastwestDist) {
					VertexNIndex midNorth;
					midNorth.index = currindex++;
					midNorth.vertex.x() = 0.5 * fourside.north.ptr0->vertex.x() + 0.5 * fourside.north.ptr1->vertex.x();
					midNorth.vertex.y() = 0.5 * fourside.north.ptr0->vertex.y() + 0.5 * fourside.north.ptr1->vertex.y();
					midNorth.vertex.z() = fourside.north.ptr0->vertex.z() + obuilding.roofHeight;
					newVerts.push_back(midNorth);

					VertexNIndex midSouth;
					midSouth.index = currindex++;
					midSouth.vertex.x() = 0.5 * fourside.south.ptr0->vertex.x() + 0.5 * fourside.south.ptr1->vertex.x();
					midSouth.vertex.y() = 0.5 * fourside.south.ptr0->vertex.y() + 0.5 * fourside.south.ptr1->vertex.y();
					midSouth.vertex.z() = fourside.south.ptr0->vertex.z() + obuilding.roofHeight;
					newVerts.push_back(midSouth);

					short quadV0Index = fourside.north.ptr0->index;//quadV0 is north0
					short quadV1Index = quadV0Index + 1;
					if (quadV1Index == roofRings.outRing[4].index) quadV1Index = roofRings.outRing[0].index;
					short quadV2Index = quadV1Index + 1;
					if (quadV2Index == roofRings.outRing[4].index) quadV2Index = roofRings.outRing[0].index;
					short quadV3Index = quadV2Index + 1;
					if (quadV3Index == roofRings.outRing[4].index) quadV3Index = roofRings.outRing[0].index;

					if (fourside.north.ptr1->index == quadV1Index)
					{
						//tri1
						newIndices.push_back(quadV0Index);
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						//tri2
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midSouth.index);
						newIndices.push_back(midNorth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri3
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midNorth.index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri4
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midSouth.index);
						newIndices.push_back(quadV2Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri5
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midNorth.index);
						newIndices.push_back(quadV1Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri6
						newIndices.push_back(quadV3Index);
						newIndices.push_back(quadV2Index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
					}
					else {
						//tri1
						newIndices.push_back(quadV3Index);
						newIndices.push_back(quadV2Index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						//tri2
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midSouth.index);
						newIndices.push_back(midNorth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri3
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midNorth.index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri4
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midSouth.index);
						newIndices.push_back(quadV1Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri5
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midNorth.index);
						newIndices.push_back(quadV0Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri6
						newIndices.push_back(quadV2Index);
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midSouth.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
					}
				}
				else {
					//east -> west main axis
					VertexNIndex midEast;
					midEast.index = currindex++;
					midEast.vertex.x() = 0.5 * fourside.east.ptr0->vertex.x() + 0.5 * fourside.east.ptr1->vertex.x();
					midEast.vertex.y() = 0.5 * fourside.east.ptr0->vertex.y() + 0.5 * fourside.east.ptr1->vertex.y();
					midEast.vertex.z() = fourside.east.ptr0->vertex.z() + obuilding.roofHeight;
					newVerts.push_back(midEast);

					VertexNIndex midWest;
					midWest.index = currindex++;
					midWest.vertex.x() = 0.5 * fourside.west.ptr0->vertex.x() + 0.5 * fourside.west.ptr1->vertex.x();
					midWest.vertex.y() = 0.5 * fourside.west.ptr0->vertex.y() + 0.5 * fourside.west.ptr1->vertex.y();
					midWest.vertex.z() = fourside.west.ptr0->vertex.z() + obuilding.roofHeight;
					newVerts.push_back(midWest);

					short quadV0Index = fourside.east.ptr0->index;//quadV0 is east0
					short quadV1Index = quadV0Index + 1;
					if (quadV1Index == roofRings.outRing[4].index) quadV1Index = roofRings.outRing[0].index;
					short quadV2Index = quadV1Index + 1;
					if (quadV2Index == roofRings.outRing[4].index) quadV2Index = roofRings.outRing[0].index;
					short quadV3Index = quadV2Index + 1;
					if (quadV3Index == roofRings.outRing[4].index) quadV3Index = roofRings.outRing[0].index;

					if (fourside.east.ptr1->index == quadV1Index)
					{
						//tri1
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midEast.index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri2
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midWest.index);
						newIndices.push_back(quadV2Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri3
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midWest.index);
						newIndices.push_back(midEast.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri4
						newIndices.push_back(quadV0Index);
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						//tri5
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midEast.index);
						newIndices.push_back(quadV1Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri6
						newIndices.push_back(quadV3Index);
						newIndices.push_back(quadV2Index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
					}
					else {
						//tri1
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midEast.index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri2
						newIndices.push_back(quadV0Index);
						newIndices.push_back(midWest.index);
						newIndices.push_back(quadV1Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri3
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midWest.index);
						newIndices.push_back(midEast.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(1);
						//tri4
						newIndices.push_back(quadV3Index);
						newIndices.push_back(quadV2Index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						//tri5
						newIndices.push_back(quadV3Index);
						newIndices.push_back(midEast.index);
						newIndices.push_back(quadV0Index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
						colorPercents.push_back(0.75);
						//tri6
						newIndices.push_back(quadV2Index);
						newIndices.push_back(quadV1Index);
						newIndices.push_back(midWest.index);
						colorPercents.push_back(0.75);
						colorPercents.push_back(0.75);
						colorPercents.push_back(1);
					}
				}
				roofok = true;
			}
		}
		else if (obuilding.roofShape.find("pyram") != string::npos || obuilding.roofShape == "cone")
		{
			double sumx = 0;
			double sumy = 0;
			for (int iv = 0; iv < roofRings.outRing.size() - 1; ++iv)
			{
				const osg::Vec3d& vert = roofRings.outRing[iv].vertex;
				sumx += vert.x();
				sumy += vert.y();
			}
			VertexNIndex vni;
			vni.vertex.x() = sumx / (roofRings.outRing.size() - 1);
			vni.vertex.y() = sumy / (roofRings.outRing.size() - 1);
			vni.vertex.z() = roofRings.outRing[0].vertex.z() + obuilding.roofHeight;
			vni.index = currindex++;
			newVerts.push_back(vni);
			//generate triangles
			// ...   ...    ...
			// v1   newv    ...
			// v0    ...    ...
			for (int iv = 0; iv < roofRings.outRing.size() - 1; ++iv)
			{
				int ivtail = iv + 1;
				if (ivtail == roofRings.outRing.size() - 1) ivtail = 0;
				const VertexNIndex& vh0 = roofRings.outRing[iv];
				const VertexNIndex& vt0 = roofRings.outRing[ivtail];
				newIndices.push_back(vh0.index);
				newIndices.push_back(vni.index);
				newIndices.push_back(vt0.index);
				colorPercents.push_back(0.75);
				colorPercents.push_back(1);
				colorPercents.push_back(0.75);
			}
			roofok = true;
		}
		else if (obuilding.roofShape == "skillion" || obuilding.roofShape == "sawtooth")
		{
			//出现了多余5个点的建筑，638320806，仔细观察发现它的某一条边上多余两个点，中间点大部分其实也是在边上的。
			//这里引入一个函数，精简这些中间点，尝试将ring缩减到5个点。
			vector<VertexNIndex> simpleRing = roofRings.outRing;
			if (roofRings.outRing.size() > 5) {
				simpleRing = trySimpleizePolygon(roofRings.outRing);
			}
			//只做5个点的建筑倾斜屋顶
			if (simpleRing.size() == 5) {
				modifySkillionRoofVertexHeightByRoofDirection(
					obuilding.roofDirection,
					obuilding.roofHeight,
					simpleRing
				);
				VertexNIndex& vh0 = simpleRing[0];
				VertexNIndex& vt0 = simpleRing[3];
				VertexNIndex& vh1 = simpleRing[1];
				VertexNIndex& vt1 = simpleRing[2];
				newIndices.push_back(vh0.index);
				newIndices.push_back(vt0.index);
				newIndices.push_back(vh1.index);
				newIndices.push_back(vh1.index);
				newIndices.push_back(vt0.index);
				newIndices.push_back(vt1.index);

				colorPercents.push_back(1);
				colorPercents.push_back(1);
				colorPercents.push_back(1);
				colorPercents.push_back(1);
				colorPercents.push_back(1);
				colorPercents.push_back(1);
				//reset roofRing values
				for (int ivroof = 0; ivroof < roofRings.outRing.size() - 1; ++ivroof) {
					for (int ivs = 0; ivs < simpleRing.size() - 1; ++ivs) {
						if (roofRings.outRing[ivroof].index == simpleRing[ivs].index) {
							roofRings.outRing[ivroof].vertex = simpleRing[ivs].vertex;
							break;
						}
					}
				}
				roofok = true;
			}
		}
		else if (obuilding.roofShape == "round")
		{
			//只做5个点的建筑倾斜屋顶
			if (roofRings.outRing.size() == 5) {
				VertexNIndex& vh0 = roofRings.outRing[0];
				VertexNIndex& vt0 = roofRings.outRing[3];
				VertexNIndex& vh1 = roofRings.outRing[1];
				VertexNIndex& vt1 = roofRings.outRing[2];
				double dx = (vt0.vertex.x() - vh0.vertex.x()) / 5;
				double dy = (vt0.vertex.y() - vh0.vertex.y()) / 5;
				double curve[6] = { 0.0, 0.59, 0.95 , 0.95 , 0.59, 0.0 };
				double colorper[6] = { 0.75, 0.59, 0.95 , 0.95 , 0.59, 0.75 };
				vector<unsigned short> tempindices;
				vector<float> tempcolors;
				for (int iadd = 1; iadd < 5; ++iadd) {
					VertexNIndex vni0, vni1;
					vni0.vertex.x() = vh0.vertex.x() + iadd * dx;
					vni0.vertex.y() = vh0.vertex.y() + iadd * dy;
					vni0.vertex.z() = vh0.vertex.z() + obuilding.roofHeight * curve[iadd];
					vni0.index = currindex++;
					newVerts.push_back(vni0);
					tempindices.push_back(vni0.index);
					tempcolors.push_back(colorper[iadd]);
					vni1.vertex.x() = vh1.vertex.x() + iadd * dx;
					vni1.vertex.y() = vh1.vertex.y() + iadd * dy;
					vni1.vertex.z() = vni0.vertex.z();
					vni1.index = currindex++;
					newVerts.push_back(vni1);
					tempindices.push_back(vni1.index);
					tempcolors.push_back(colorper[iadd]);
				}

				//top faces
				newIndices.push_back(vh0.index);
				newIndices.push_back(tempindices[0]);
				newIndices.push_back(tempindices[1]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[0]);
				colorPercents.push_back(tempcolors[1]);

				newIndices.push_back(vh0.index);
				newIndices.push_back(tempindices[1]);
				newIndices.push_back(vh1.index);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[1]);
				colorPercents.push_back(0.75);

				for (int iquad = 0; iquad < 3; ++iquad) {
					newIndices.push_back(tempindices[iquad * 2 + 0]);
					newIndices.push_back(tempindices[iquad * 2 + 2]);
					newIndices.push_back(tempindices[iquad * 2 + 3]);
					colorPercents.push_back(tempcolors[iquad * 2 + 0]);
					colorPercents.push_back(tempcolors[iquad * 2 + 2]);
					colorPercents.push_back(tempcolors[iquad * 2 + 3]);

					newIndices.push_back(tempindices[iquad * 2 + 0]);
					newIndices.push_back(tempindices[iquad * 2 + 3]);
					newIndices.push_back(tempindices[iquad * 2 + 1]);
					colorPercents.push_back(tempcolors[iquad * 2 + 0]);
					colorPercents.push_back(tempcolors[iquad * 2 + 3]);
					colorPercents.push_back(tempcolors[iquad * 2 + 1]);
				}

				newIndices.push_back(tempindices[6]);
				newIndices.push_back(vt0.index);
				newIndices.push_back(vt1.index);
				colorPercents.push_back(tempcolors[6]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(0.75);

				newIndices.push_back(tempindices[6]);
				newIndices.push_back(vt1.index);
				newIndices.push_back(tempindices[7]);
				colorPercents.push_back(tempcolors[6]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[7]);

				//side faces
				newIndices.push_back(vh0.index);//tri
				newIndices.push_back(vt0.index);
				newIndices.push_back(tempindices[0]);

				colorPercents.push_back(0.75);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[0]);

				newIndices.push_back(tempindices[0]);//tri
				newIndices.push_back(vt0.index);
				newIndices.push_back(tempindices[2]);

				colorPercents.push_back(tempcolors[0]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[2]);

				newIndices.push_back(tempindices[2]);//tri
				newIndices.push_back(vt0.index);
				newIndices.push_back(tempindices[4]);

				colorPercents.push_back(tempcolors[2]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[4]);

				newIndices.push_back(tempindices[4]);//tri
				newIndices.push_back(vt0.index);
				newIndices.push_back(tempindices[6]);

				colorPercents.push_back(tempcolors[4]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[6]);

				newIndices.push_back(vh1.index);//tri
				newIndices.push_back(tempindices[1]);
				newIndices.push_back(vt1.index);

				colorPercents.push_back(0.75);
				colorPercents.push_back(tempcolors[1]);
				colorPercents.push_back(0.75);

				newIndices.push_back(tempindices[1]);//tri
				newIndices.push_back(tempindices[3]);
				newIndices.push_back(vt1.index);

				colorPercents.push_back(tempcolors[1]);
				colorPercents.push_back(tempcolors[3]);
				colorPercents.push_back(0.75);

				newIndices.push_back(tempindices[3]);//tri
				newIndices.push_back(tempindices[5]);
				newIndices.push_back(vt1.index);

				colorPercents.push_back(tempcolors[3]);
				colorPercents.push_back(tempcolors[5]);
				colorPercents.push_back(0.75);

				newIndices.push_back(tempindices[5]);//tri
				newIndices.push_back(tempindices[7]);
				newIndices.push_back(vt1.index);

				colorPercents.push_back(tempcolors[5]);
				colorPercents.push_back(tempcolors[7]);
				colorPercents.push_back(0.75);
				roofok = true;
			}
		}
		else if (obuilding.roofShape == "dome" || obuilding.roofShape == "onion")
		{
			osg::Vec2d tcenter = computeNVIRingCenter(roofRings.outRing);
			vector<vector<unsigned short>> indicesArray(roofRings.outRing.size() - 1);
			vector<vector<float>> cccArr(roofRings.outRing.size() - 1);
			VertexNIndex centerVNI;
			centerVNI.vertex.x() = tcenter.x();
			centerVNI.vertex.y() = tcenter.y();
			centerVNI.vertex.z() = roofRings.outRing[0].vertex.z() + obuilding.roofHeight;
			centerVNI.index = currindex++;
			newVerts.push_back(centerVNI);
			double curve[5] = { 0.000, 0.600, 0.800, 0.917, 0.980 };//from edge to center
			float colorper[5] = { 0.80 , 0.85, 0.90 , 0.95 , 0.98 };
			for (int iv = 0; iv < roofRings.outRing.size() - 1; ++iv)
			{
				double dx = (centerVNI.vertex.x() - roofRings.outRing[iv].vertex.x()) / 5;
				double dy = (centerVNI.vertex.y() - roofRings.outRing[iv].vertex.y()) / 5;
				for (int iadd = 1; iadd < 5; ++iadd) {
					VertexNIndex newVni;
					newVni.vertex.x() = roofRings.outRing[iv].vertex.x() + iadd * dx;
					newVni.vertex.y() = roofRings.outRing[iv].vertex.y() + iadd * dy;
					newVni.vertex.z() = roofRings.outRing[iv].vertex.z() + obuilding.roofHeight * curve[iadd];
					newVni.index = currindex++;
					newVerts.push_back(newVni);
					indicesArray[iv].push_back(newVni.index);
					cccArr[iv].push_back(colorper[iadd]);
				}
			}
			//tris
			for (int iv = 0; iv < roofRings.outRing.size() - 1; ++iv)
			{
				int ivnext = iv + 1;
				if (ivnext == roofRings.outRing.size() - 1) ivnext = 0;
				newIndices.push_back(roofRings.outRing[iv].index);
				newIndices.push_back(indicesArray[iv][0]);
				newIndices.push_back(indicesArray[ivnext][0]);
				colorPercents.push_back(0.75);
				colorPercents.push_back(cccArr[iv][0]);
				colorPercents.push_back(cccArr[ivnext][0]);

				newIndices.push_back(roofRings.outRing[iv].index);
				newIndices.push_back(indicesArray[ivnext][0]);
				newIndices.push_back(roofRings.outRing[ivnext].index);
				colorPercents.push_back(0.75);
				colorPercents.push_back(cccArr[ivnext][0]);
				colorPercents.push_back(0.75);

				for (int iquad = 0; iquad < 3; ++iquad) {
					newIndices.push_back(indicesArray[iv][iquad]);
					newIndices.push_back(indicesArray[iv][iquad + 1]);
					newIndices.push_back(indicesArray[ivnext][iquad + 1]);

					colorPercents.push_back(cccArr[iv][iquad]);
					colorPercents.push_back(cccArr[iv][iquad + 1]);
					colorPercents.push_back(cccArr[ivnext][iquad + 1]);

					newIndices.push_back(indicesArray[iv][iquad]);
					newIndices.push_back(indicesArray[ivnext][iquad + 1]);
					newIndices.push_back(indicesArray[ivnext][iquad]);

					colorPercents.push_back(cccArr[iv][iquad]);
					colorPercents.push_back(cccArr[ivnext][iquad + 1]);
					colorPercents.push_back(cccArr[ivnext][iquad]);
				}

				newIndices.push_back(indicesArray[iv][3]);
				newIndices.push_back(centerVNI.index);
				newIndices.push_back(indicesArray[ivnext][3]);

				colorPercents.push_back(cccArr[iv][3]);
				colorPercents.push_back(1);
				colorPercents.push_back(cccArr[ivnext][3]);

			}

			roofok = true;
		}
	}
	if (roofok == false)
	{//flat, butterfly
		vector<p2t::Point*> outPolyline;
		for (int iv = 0; iv < roofRings.outRing.size() - 1; ++iv) {
			const VertexNIndex& vni = roofRings.outRing[iv];
			P2tPointWithIndex* pti = new P2tPointWithIndex(vni.vertex.x(), vni.vertex.y(), vni.index);
			outPolyline.push_back(pti);
		}
		vector< vector<p2t::Point*> > holeArray;
		for (int ihole = 0; ihole < roofRings.innRingVec.size(); ++ihole)
		{
			vector<p2t::Point*> onehole;
			const vector<VertexNIndex>& holeVerts = roofRings.innRingVec[ihole];
			for (int iv = 0; iv < holeVerts.size() - 1; ++iv)
			{
				const VertexNIndex& vni = holeVerts[iv];
				P2tPointWithIndex* pti = new P2tPointWithIndex(vni.vertex.x(), vni.vertex.y(), vni.index);
				onehole.push_back(pti);
			}
			holeArray.push_back(onehole);
		}
		//make triangles
		p2t::CDT* cdt = new p2t::CDT(outPolyline);
		for (int ihole = 0; ihole < holeArray.size(); ++ihole) {
			cdt->AddHole(holeArray[ihole]);
		}
		cdt->Triangulate();
		vector<p2t::Triangle*> triangles = cdt->GetTriangles();
		int numTri = triangles.size();

		//collection new indices
		for (auto it = triangles.begin(); it != triangles.end(); ++it) {
			short index0 = ((P2tPointWithIndex*)(*it)->GetPoint(0))->index;
			short index1 = ((P2tPointWithIndex*)(*it)->GetPoint(1))->index;
			short index2 = ((P2tPointWithIndex*)(*it)->GetPoint(2))->index;
			newIndices.push_back(index0);
			newIndices.push_back(index1);
			newIndices.push_back(index2);

			colorPercents.push_back(1);
			colorPercents.push_back(1);
			colorPercents.push_back(1);
		}
		delete cdt;
		for (int iv = 0; iv < outPolyline.size(); ++iv) {
			delete outPolyline[iv]; outPolyline[iv] = 0;
		}
		for (int ihole = 0; ihole < holeArray.size(); ++ihole) {
			for (int iv = 0; iv < holeArray[ihole].size(); ++iv) {
				delete holeArray[ihole][iv];
				holeArray[ihole][iv] = 0;
			}
		}
	}
}


//计算三角形法向量
//compute normals by https://stackoverflow.com/questions/16340931/calculating-vertex-normals-of-a-mesh
//Think it otherway round: Iterate over the faces and add to the normal of the vertex. 
//Once you processed all faces, normalize the vertex normal to unit length.
osg::Vec3 OSMBuildingFeature::computeOneNormal(osg::Vec3d* v0, osg::Vec3d* v1, osg::Vec3d* v2)
{
	osg::Vec3d norm = (*v1 - *v0) ^ (*v2 - *v0);
	norm.normalize();
	return norm;
}



/// <summary>
/// 指定原点构建建筑物的绘制数据
/// </summary>
/// <param name="obuildingFeature"></param>
/// <param name="originLL">经纬度原点</param>
/// <returns></returns>
void OSMBuildingFeature::buildOsmBuildingGeometryWithCenterPoint(
	OSMBuildingFeature& obuildingFeature,
	const osg::Vec2d originLL,
	const double buildingBaseHeight,
	osg::Vec3dArray* vertexArray,
	osg::Vec3Array* normalArray,
	osg::Vec3Array* colorArray
)
{
	//osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	//底部环线
	Building3DRings btm3dRings;
	computeRelativeCenterPoints(obuildingFeature, originLL, buildingBaseHeight, btm3dRings);
	//构建顶部环线
	Building3DRings top3dRings;
	generateExtrudedPoints(obuildingFeature, btm3dRings, top3dRings);
	//make wall triangle vertex indices
	vector<unsigned short> wallTriIndices;
	vector<float> colorPercents;
	makeWallTriangleVertexIndices(btm3dRings, top3dRings, wallTriIndices, colorPercents);
	//make roof triangle vertex indices
	vector<unsigned short> roofTriIndices;
	vector<float>roofColorPercents;
	vector<VertexNIndex> extraRoofVertices;//给屋顶额外追加的顶点
	makeRoofVerticesAndIndices(
		obuildingFeature,
		top3dRings,
		extraRoofVertices,
		roofTriIndices, roofColorPercents);
	//merge all verts
	vector<osg::Vec3d> mergeAllVerts;
	for (int iv = 0; iv < btm3dRings.outRing.size(); ++iv) {
		mergeAllVerts.push_back(btm3dRings.outRing[iv].vertex);
	}
	for (int ihole = 0; ihole < btm3dRings.innRingVec.size(); ++ihole) {
		for (int iv = 0; iv < btm3dRings.innRingVec[ihole].size(); ++iv) {
			mergeAllVerts.push_back(btm3dRings.innRingVec[ihole][iv].vertex);
		}
	}
	for (int iv = 0; iv < top3dRings.outRing.size(); ++iv) {
		mergeAllVerts.push_back(top3dRings.outRing[iv].vertex);
	}
	for (int ihole = 0; ihole < top3dRings.innRingVec.size(); ++ihole) {
		for (int iv = 0; iv < top3dRings.innRingVec[ihole].size(); ++iv) {
			mergeAllVerts.push_back(top3dRings.innRingVec[ihole][iv].vertex);
		}
	}
	for (int iv = 0; iv < extraRoofVertices.size(); ++iv) mergeAllVerts.push_back(extraRoofVertices[iv].vertex);
	//根据三角形顶点索引值构建全部顶点数据，这里不使用drawElements
	//osg::ref_ptr<osg::Vec3dArray> vertexArray = new osg::Vec3dArray;
	int vertexstartindex = vertexArray->size();
	for (int it = 0; it < wallTriIndices.size(); ++it) {
		vertexArray->push_back(mergeAllVerts[wallTriIndices[it]]);
	}
	//this is vec3d count, not byte size. 
	// this is same as vertexArray->getNumElements().
	int roofVertex0Index = vertexArray->size();
	for (int it = 0; it < roofTriIndices.size(); ++it) {
		vertexArray->push_back(mergeAllVerts[roofTriIndices[it]]);
		colorPercents.push_back(roofColorPercents[it]);
	}
	//每三个点计算法向量
	//osg::ref_ptr<osg::Vec3Array> normalArray = new osg::Vec3Array;
	//osg::ref_ptr<osg::Vec3Array> colorArray = new osg::Vec3Array;
	int numTri = (wallTriIndices.size() + roofTriIndices.size()) / 3;
	for (int itri = 0; itri < numTri; ++itri) {
		osg::Vec3d* v0 = (osg::Vec3d*)vertexArray->getDataPointer(vertexstartindex + itri * 3 + 0);
		osg::Vec3d* v1 = (osg::Vec3d*)vertexArray->getDataPointer(vertexstartindex + itri * 3 + 1);
		osg::Vec3d* v2 = (osg::Vec3d*)vertexArray->getDataPointer(vertexstartindex + itri * 3 + 2);

		float color0 = colorPercents[itri * 3 + 0];
		float color1 = colorPercents[itri * 3 + 1];
		float color2 = colorPercents[itri * 3 + 2];

		osg::Vec3 norm = computeOneNormal(v0, v1, v2);
		normalArray->push_back(norm);
		normalArray->push_back(norm);
		normalArray->push_back(norm);
		if (vertexstartindex + itri * 3 < roofVertex0Index) {
			//wall color
			colorArray->push_back(obuildingFeature.color * color0);
			colorArray->push_back(obuildingFeature.color * color1);
			colorArray->push_back(obuildingFeature.color * color2);
		}
		else {
			//roof color
			colorArray->push_back(obuildingFeature.roofColor * color0);
			colorArray->push_back(obuildingFeature.roofColor * color1);
			colorArray->push_back(obuildingFeature.roofColor * color2);
		}
	}
}