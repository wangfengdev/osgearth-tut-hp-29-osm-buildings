#pragma once

//triangular mesh
#include "poly2tri/poly2tri.h"

//this class is only used for generate triangle mesh from a ring of polygon.
struct P2tPointWithIndex :public p2t::Point {
public:
	P2tPointWithIndex() :p2t::Point() {}
	P2tPointWithIndex(double x1, double y1, short index1) :p2t::Point(x1, y1), index(index1) {}
	short index = 0;
};
