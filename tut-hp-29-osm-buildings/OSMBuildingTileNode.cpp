#include "OSMBuildingTileNode.h"

osgEarth::SpatialReference* OSMBuildingTileNode::_s_srs = osgEarth::SpatialReference::get("wgs84");

OSMBuildingTileNode::OSMBuildingTileNode( 
    const string& name, double centerLon, double centerLat, string& jsonTextOrOgrFilename)
    : osg::MatrixTransform()
	, _name(name)
    , _tileCenterPoint(centerLon, centerLat,0)
    , _geode(nullptr)
    , _geometry(nullptr)
{
    //1 reading building datas
    OSMBuildingFeature::ReadBuildingFeaturesFromJsonText(jsonTextOrOgrFilename, _osmBuildingFeatureArray);
    //2 计算json全部建筑的中心点
    //osg::Vec2d jsonCenter = OSMBuildingFeature::computeAllBuildingsCenter(_osmBuildingFeatureArray);
    //use tile center lonlat rather than all buildings center for GeoTransform.
    osg::Vec2d jsonCenter(centerLon, centerLat);
    _currentBuildingHeiVsTileHeiArrray.resize(_osmBuildingFeatureArray.size(), 0);
    for (int ib = 0; ib < _osmBuildingFeatureArray.size(); ++ib) {
        osg::Vec2 oneBuildingCenter = OSMBuildingFeature::computeBuildingCenter(_osmBuildingFeatureArray[ib]);
        _osmBuildingCenter2DArray.push_back(oneBuildingCenter);
    }
    _tileCenterPoint.z() = 0;
    buildTheTileNode();

    /// set MatrixTransform staff
    // this->setPosition(osgEarth::GeoPoint(osgEarth::SpatialReference::get("wgs84"), _tileCenterPoint.x(), _tileCenterPoint.y()));
    this->setMatrixTransform(this->getTileCenter());
    
    //set the update callback
    osg::ref_ptr<UpdateCallback> updater = new UpdateCallback(this);
    this->addUpdateCallback(updater.get());

}

//new for pbf
OSMBuildingTileNode::OSMBuildingTileNode(const string& name, double centerLon, double centerLat, string& pbfdata, const osgEarth::TileKey& key)
    : osg::MatrixTransform()
    , _name(name)
    , _tileCenterPoint(centerLon, centerLat, 0)
    , _geode(nullptr)
    , _geometry(nullptr)
{



    //1 reading building datas
    //OSMBuildingFeature::ReadBuildingFeaturesFromJsonText(jsonTextOrOgrFilename, _osmBuildingFeatureArray);
    bool readok = OSMBuildingFeature::ReadBuildingFeaturesFromPBF(
        pbfdata,
        key, _osmBuildingFeatureArray);

    //2 计算json全部建筑的中心点
    //osg::Vec2d jsonCenter = OSMBuildingFeature::computeAllBuildingsCenter(_osmBuildingFeatureArray);
    //use tile center lonlat rather than all buildings center for GeoTransform.
    osg::Vec2d jsonCenter(centerLon, centerLat);
    _currentBuildingHeiVsTileHeiArrray.resize(_osmBuildingFeatureArray.size(), 0);
    for (int ib = 0; ib < _osmBuildingFeatureArray.size(); ++ib) {
        osg::Vec2 oneBuildingCenter = OSMBuildingFeature::computeBuildingCenter(_osmBuildingFeatureArray[ib]);
        _osmBuildingCenter2DArray.push_back(oneBuildingCenter);
    }
    _tileCenterPoint.z() = 0;
    buildTheTileNode();

    /// set MatrixTransform staff
    // this->setPosition(osgEarth::GeoPoint(osgEarth::SpatialReference::get("wgs84"), _tileCenterPoint.x(), _tileCenterPoint.y()));
    this->setMatrixTransform(this->getTileCenter());

    //set the update callback
    osg::ref_ptr<UpdateCallback> updater = new UpdateCallback(this);
    this->addUpdateCallback(updater.get());

}


void OSMBuildingTileNode::setMatrixTransform(osg::Vec3d v)
{
    osgEarth::GeoPoint p(_s_srs, v.x(),v.y(),v.z(), osgEarth::AltitudeMode::ALTMODE_ABSOLUTE); 
    // no need p.makeAbsolute() maybe
    osg::Matrixd local2world;
    p.createLocalToWorld(local2world);
    this->setMatrix(local2world);
}


OSMBuildingTileNode::~OSMBuildingTileNode()
{
    osg::MatrixTransform::~MatrixTransform();
}

void OSMBuildingTileNode::buildTheTileNode()
{
    osg::Vec2d jsonCenter(_tileCenterPoint.x(), _tileCenterPoint.y());
    //3 make building geometies
    osg::ref_ptr<osg::Vec3dArray> vertexArray = new osg::Vec3dArray;
    osg::ref_ptr<osg::Vec3Array> normalArray = new osg::Vec3Array;
    osg::ref_ptr<osg::Vec3Array> colorArray = new osg::Vec3Array;
    _vertexBuildingIndexArray.clear();
    double tilecenterheight = _tileCenterPoint.z();
    for (int ifea = 0; ifea < _osmBuildingFeatureArray.size(); ++ifea)
    {
        unsigned int index0 = vertexArray->size() ; 
        OSMBuildingFeature::buildOsmBuildingGeometryWithCenterPoint(
            _osmBuildingFeatureArray[ifea],
            jsonCenter,
            _currentBuildingHeiVsTileHeiArrray[ifea],
            vertexArray.get(), normalArray.get(), colorArray.get());
        for (unsigned int iv = index0; iv < vertexArray->size(); ++iv) {
            _vertexBuildingIndexArray.push_back(ifea);
        }
    }
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
    geom->setVertexArray(vertexArray.get());
    geom->setNormalArray(normalArray.get(), osg::Array::Binding::BIND_PER_VERTEX);
    geom->setColorArray(colorArray.get(), osg::Array::Binding::BIND_PER_VERTEX);
    geom->getOrCreateStateSet()->setMode(GL_CULL_FACE, osg::StateAttribute::ON);
    geom->getOrCreateStateSet()->setAttribute(new osg::CullFace(osg::CullFace::BACK));
    geom->getOrCreateStateSet()->setAttribute(
        new osg::FrontFace(osg::FrontFace::COUNTER_CLOCKWISE));
    osg::ref_ptr<osg::DrawArrays> drawArrayPrimitiveSet = new osg::DrawArrays(GL_TRIANGLES, 0, vertexArray->size() );
    geom->addPrimitiveSet(drawArrayPrimitiveSet.get());
    if (_geode.valid()) {
        this->removeChild(_geode.get());
        _geode = nullptr;
    }
    _geode = new osg::Geode;
    _geode->addDrawable(geom.get());
    _geometry = geom;
    this->addChild(_geode.get());
}

void OSMBuildingTileNode::traverse(osg::NodeVisitor& nv) //for install TerrainCallback
{
    osg::MatrixTransform::traverse(nv);
}

 



//更新高程数据
void OSMBuildingTileNode::UpdateCallback::operator()(osg::Node* node, osg::NodeVisitor* nv)
{
    osg::ref_ptr<OSMBuildingTileNode> tileNode;
    if (_tileNode.lock(tileNode)) {
        if (tileNode->isNeedUpdateHeight())
        {
            tileNode->recomputeBuildingHeights();
        }
    }
    // must call any nested node callbacks and continue subgraph traversal.
    NodeCallback::traverse(node, nv);
}


bool OSMBuildingTileNode::setNeedUpdateHeight(double tileHeight, vector<double>& newBuiHeiArr) {
    if (_needUpdateHeights == false) {
        _tileCenterPoint.z() = tileHeight;
        _newBuiHeightArray = newBuiHeiArr;
        _needUpdateHeights = true;
        return true;
    }
    else {
        return false;
    }
}

void  OSMBuildingTileNode::recomputeBuildingHeights()
{
    osg::Vec3dArray* vertices = (osg::Vec3dArray*)(getGeometry()->getVertexArray());
    int num = vertices->size();
    osg::Vec3d* cursor = (osg::Vec3d*)vertices->getDataPointer();
    for (int iv = 0; iv < num; ++iv) {
        int buiIndex = _vertexBuildingIndexArray[iv];
        cursor->z() = cursor->z() - _currentBuildingHeiVsTileHeiArrray[buiIndex] + _newBuiHeightArray[buiIndex];
        cursor++;
    }
    _geometry->dirtyDisplayList();//this is important!!!
    _currentBuildingHeiVsTileHeiArrray = _newBuiHeightArray;
    setMatrixTransform(_tileCenterPoint);
    _needUpdateHeights = false;
    dirtyBound();
}